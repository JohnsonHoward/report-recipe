import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.link.IssueLink
import com.atlassian.jira.issue.link.IssueLinkManager
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.xml.MarkupBuilder
import com.atlassian.jira.issue.IssueManager

//Issue issue = issue
IssueManager issueManager = ComponentAccessor.getIssueManager()
Issue issue = issueManager.getIssueByCurrentKey("TEST-1")
return doReport(issue)

//start the report generation
def doReport(Issue issue){

    def worked = null
    def result = null
    def spaceKey = "REP"
    def spaceTitle = "Reports"

    if (spaceExists(spaceKey) != HttpURLConnection.HTTP_OK) {
        createSpace(spaceKey)
    }

    def resultPageExists = pageExists(spaceKey, issue.key)

    if(resultPageExists?.statusCode == HttpURLConnection.HTTP_OK){
        result = createPage(issue)

    }

    return result

}
//check the page exists
def pageExists(String key, String id) {

    return sendGetRequest("/content?spaceKey="+key+"&title="+ id+"&expand=version")
}

//check space exists
def spaceExists(String key) {

    def response = sendGetRequest("/space/" + key) as Response
    return response.statusCode
}
//create space
def createSpace(String key) {

    LinkedHashMap<String, Serializable> params = [key:key,name:key]
    return sendPostRequest("/space/", params)
}
//create a page
def createPage(Issue issue) {
    LinkedHashMap<String, Serializable> params = getParams(issue)
    def response = sendPostRequest("/content", params) as Response
    return response.statusCode
}
def getParams(Issue issue){

    def spaceKey = "REP"
    def pageTitle = issue.key

    LinkedHashMap<String, Serializable> params = [
            type : "page",
            version: [number:1],
            title: pageTitle,
            space: [
                    key: spaceKey // set the space key - or calculate it from the project or something
            ],
            body : [
                    storage: [
                            value         :getReport(issue).toString() + getJiraLink(issue).toString(),
                            representation: "storage"
                    ]
            ]
    ]

    return params
}
//get the report
def getReport(Issue issue){

    def reportPath = getReportPath()
    def getResult = sendGetRequest(reportPath)
    def jsonSlurper = new JsonSlurper()
    def body = getResult.getResponseBodyAsString()
    def object = jsonSlurper.parseText(body) as Map
    def pageResult = object.results.body.view.value as ArrayList<String>
    return replaceReportText(pageResult.toString(), issue)

}
//get the report path
def getReportPath(){

    def path = "Report"
    return "/content?type=page&spaceKey=SR&title="+path+"&expand=space,body.view,version,container"

}

//replace the report body placeholders
def replaceReportText(String xml, Issue issue){

    // CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
    // String cf = issue.getCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Market SPID")).toString()
    def newXML = xml.replace("PHReporter",issue.reporter.toString())
    return newXML
}


//check if ticket has infringements
def getLinkedIssueData(Issue issue){

    IssueLinkManager issueLinkManager = ComponentAccessor.getIssueLinkManager()
    List<IssueLink> linkedIssue = issueLinkManager.getOutwardLinks(issue.id).findAll{ it.issueLinkType.inward == "Infringement for inspection"}

    return linkedIssue

}
def getJiraLink(Issue issue){

    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer)

    xml.'ac:structured-macro' ('ac:name': "jira", 'ac:schema-version': "1", 'ac:macro-id': "ce876bed-eeb4-4592-9255-a3a216c1d507") {
        'ac:parameter' ('ac:name': "key", issue.key)
        'ac:parameter' ('ac:name': "server", "System JIRA")
        'ac:parameter' ('ac:name': "serverId", "BF2X-MIW5-AC43-7B9I")

    }

    return writer
}

def getTable(def list){

    CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
    CustomField cf1 = customFieldManager.getCustomFieldObjectByName("Number of Occurences")
    def writer = new StringWriter()
    def xml = new MarkupBuilder(writer)
    xml.table {
        tr {
            th("Item")
        }
        list.each{ item ->
            tr{
                td(item.key)
            }
        }
    }

    return writer.toString()
}
//send get request for url
def sendGetRequest(String request) {
    def url = "https://avst-test1702.adaptavist.cloud/rest/api" + request
    def responseWorked = null
    ApplicationLink confluenceLink = getPrimaryConfluenceLink()
    if (!confluenceLink) {
        return null
    }// must have a working app link set up
    def authenticatedRequestFactory = confluenceLink.createAuthenticatedRequestFactory()
    authenticatedRequestFactory
            .createRequest(Request.MethodType.GET, url)
            .addHeader("Content-Type", "application/json")
            .execute(new ResponseHandler<Response>() {
        @Override
        void handle(Response response) throws ResponseException {

            responseWorked = response;

        }
    })
    return responseWorked
}

//send post request for params
def sendPostRequest(String request, LinkedHashMap<String, Serializable> params) {

    def url = "https://avst-test1702.adaptavist.cloud/rest/api" + request

    def responseWorked = null
    ApplicationLink confluenceLink = getPrimaryConfluenceLink()
    if (!confluenceLink) {
        return null
    }
    confluenceLink.createImpersonatingAuthenticatedRequestFactory()
    def authenticatedRequestFactory = confluenceLink.createAuthenticatedRequestFactory()
    String response = authenticatedRequestFactory
            .createRequest(Request.MethodType.POST, url)
            .addHeader("Content-Type", "application/json")
            .setRequestBody(new JsonBuilder(params).toString())
            .execute(new ResponseHandler<Response>() {
        @Override
        void handle(Response response) throws ResponseException {
            responseWorked = response;
        }
    })
    return responseWorked
}

//get application link
def getPrimaryConfluenceLink() {
    def applicationLinkService = ComponentLocator.getComponent(ApplicationLinkService.class)
    final ApplicationLink conflLink = applicationLinkService.getPrimaryApplicationLink(ConfluenceApplicationType.class);
    conflLink
}






/**
 * Created by jhoward on 04/01/2018.
 */
